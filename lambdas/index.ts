import {unmarshall} from '@aws-sdk/util-dynamodb'

export const handler = async (event: any) => {
    console.log("hello invoked");
    //console.log(JSON.stringify(event));
    const record = event.Records[0];
    const body = JSON.parse(record.body);
    console.log(body);
    console.log(unmarshall(body.dynamodb.OldImage));
};