import { DeleteItemCommandInput } from "@aws-sdk/client-dynamodb";

const { DynamoDBClient, ScanCommand, BatchWriteItemCommand, } = require("@aws-sdk/client-dynamodb");

const client = new DynamoDBClient({ region: "us-east-1" });

export const handler = async (event: any) => {
    try {
        // Scan the table to fetch all records for the given customer
        const scanParams = {
            TableName: "claim-history", // Replace "YOUR_TABLE_NAME" with your table name
            FilterExpression: "customer_id = :cid",
            ExpressionAttributeValues: {
                ":cid": { S: "Customer_1" }
            }
        };
        const scanCommand = new ScanCommand(scanParams);
        const scanResults = await client.send(scanCommand);
        console.log("scanResults",JSON.stringify(scanResults));
        // Prepare the list of delete operations for the items where "resolve" attribute does not exist
        const deleteRequests = scanResults.Items
            .filter((item:any) => !item.temped)
            .map((item:any) => ({
                DeleteRequest: {
                    Key: {
                        "customer_id": { S: "Customer_1" },
                        "order_id": { S: item.order_id.S }// Assuming order_id is a string
                    }
                }
            }));

        // Execute the batch delete operation within a transaction
        if (deleteRequests.length > 0) {
            const batchSize = 25; // Batch size limit for batchWriteItem is 25
            for (let i = 0; i < deleteRequests.length; i += batchSize) {
                const batchItems = deleteRequests.slice(i, i + batchSize);
                const batchParams = {
                    RequestItems: {
                        "claim-history": batchItems
                    }
                };
                const batchCommand = new BatchWriteItemCommand(batchParams);
                await client.send(batchCommand);
            }
            console.log("Records deleted successfully.");
        } else {
            console.log("No records to delete.");
        }
    } catch (error) {
        console.error("Error deleting records:", error);
    }
   
   
    // const item: DeleteItemCommandInput = {
    //     TableName: "claim-history",
    //     Key: {
    //         "customer_id": { "S": "Customer_1" },
    //     },
    //     ConditionExpression: "attribute_not_exists(temped)"
    // };  
    // const command = new DeleteItemCommand(item);
    // // Execute the command
    // try {
    //     const response = await client.send(command);
    //     console.log({response});
    // } catch (error) {
    //     console.error("Error deleting item:", error);
    // }
}; 