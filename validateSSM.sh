#!/bin/bash

# Variable to keep track of any invalid parameters
INVALID_PARAM_FOUND=false

# Iterate over each parameter name passed as arguments
for PARAM_NAME; do
    # Retrieve the parameter value from AWS SSM Parameter Store
    PARAM_VALUE=$(aws ssm get-parameter --name "$PARAM_NAME" --query 'Parameter.Value' --with-decryption --output text)

    # Check if the parameter value is valid JSON
    if ! jq empty <<< "$PARAM_VALUE" > /dev/null 2>&1; then
        echo "Parameter value for $PARAM_NAME is not in valid JSON format!"
        INVALID_PARAM_FOUND=true
        # Continue to the next iteration without printing anything
        continue
    fi

    echo "Checking parameter $PARAM_NAME..."
    echo "Parameter value for $PARAM_NAME is in valid JSON format."
done

# Check if any invalid parameter was found
if [ "$INVALID_PARAM_FOUND" = true ]; then
    echo "Some parameters were not in valid JSON format."
    exit 1
fi
