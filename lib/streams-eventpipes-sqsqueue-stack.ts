import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as dynamodb from 'aws-cdk-lib/aws-dynamodb';
import * as event_pipes from 'aws-cdk-lib/aws-pipes';
import * as sqs from 'aws-cdk-lib/aws-sqs';
import * as iam from 'aws-cdk-lib/aws-iam';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as lambda_eventsources from 'aws-cdk-lib/aws-lambda-event-sources';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class StreamsEventpipesSqsqueueStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    // Define DynamoDB table
    const table = new dynamodb.Table(this, 'streamTableToPipes', {
      tableName: "streamTableToPipes",
      partitionKey: { name: 'ID', type: dynamodb.AttributeType.STRING },
      stream: dynamodb.StreamViewType.NEW_AND_OLD_IMAGES, // Enable streams with desired view type
    });


    // QUEUE
    const queue = new sqs.Queue(this, "sqsQueue", {
      queueName: "QueueCollectEventsFromStream"
    });
    const eventpipeDLQ = new sqs.Queue(this, "eventPipeDlq", {
      queueName: "eventPipeDlq"
    });


    // PIPE ROLES

    const pipeSourcePolicy = new iam.PolicyStatement({
      actions: ['dynamodb:*'],
      effect: iam.Effect.ALLOW,
      resources: [table.tableStreamArn as string]
    })
    const pipeTargetPolicy = new iam.PolicyStatement({
      actions: ['sqs:*'],
      effect: iam.Effect.ALLOW,
      resources: [queue.queueArn, eventpipeDLQ.queueArn]
    })

    const eventPipeRole = new iam.Role(this, "eventPipeRole", {
      roleName: "eventPipeRole",
      assumedBy: new iam.ServicePrincipal("pipes.amazonaws.com")
    })
    eventPipeRole.addToPolicy(pipeSourcePolicy);
    eventPipeRole.addToPolicy(pipeTargetPolicy);


    // PIPE
    const eventPipe = new event_pipes.CfnPipe(this, "streamToSQSqueue", {
      source: table.tableStreamArn as string,
      target: queue.queueArn,
      roleArn: eventPipeRole.roleArn,
      name: "streamToSQSqueue",
      sourceParameters: {
        dynamoDbStreamParameters: {
          startingPosition: "LATEST",
          batchSize: 10,
          onPartialBatchItemFailure: "AUTOMATIC_BISECT",
          maximumRetryAttempts: 3,
          deadLetterConfig: {
            arn: eventpipeDLQ.queueArn
          }
        },
        filterCriteria:{
          filters:[{
            pattern: '{\"eventName\": [\"REMOVE\"]}'
          }]
        }
      }
    })


    // LAMBDA ROLE

    const lambdaRole = new iam.Role(this, 'ReadFromQueueRole', {
      assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
      roleName: "ReadFromQueueRole"
    });
    lambdaRole.addToPolicy(new iam.PolicyStatement({
      actions: ['sqs:*'],
      effect: iam.Effect.ALLOW,
      resources: [queue.queueArn]
    }))
    const lambdaFunction = new lambda.Function(this, 'MyLambda', {
      functionName: 'ReadFromQueue',
      runtime: lambda.Runtime.NODEJS_18_X,
      handler: 'index.handler',
      code: lambda.Code.fromAsset('lambdas'),
      role: lambdaRole
    });

    lambdaRole.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaBasicExecutionRole'));
    lambdaFunction.addEventSource((new lambda_eventsources.SqsEventSource(queue, {
      batchSize: 1,
      maxBatchingWindow: cdk.Duration.seconds(1)
    })));

    const lambdaFunction1 = new lambda.Function(this, 'MyLambda1', {
      functionName: 'DeleteDynamoRecord',
      runtime: lambda.Runtime.NODEJS_18_X,
      handler: 'delete.handler',
      code: lambda.Code.fromAsset('lambdas'),
      role: lambdaRole
    });
  }
}
