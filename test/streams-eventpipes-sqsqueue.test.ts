const AWS = require('aws-sdk');
const assert = require('assert');

// Initialize AWS SDK with your credentials and region
AWS.config.update({region:"us-east-1"});

// Create an instance of the SSM service
const ssm = new AWS.SSM();

// Function to retrieve the value of an SSM parameter
async function getSSMParameterValue(parameterName:string) {
  const params = {
    Name: parameterName,
    WithDecryption: true // Decrypt the parameter value if it's encrypted
  };

  try {
    const response = await ssm.getParameter(params).promise();
    console.log(response);
    return response.Parameter.Value;
  } catch (error) {
    throw error;
  }
}

// Test case to check the actual value of an SSM parameter
describe('SSM Parameter Value Test', () => {
  it('should retrieve the correct value of the SSM parameter', async () => {
    const parameterName = '/yashpandey';
    try {
      const actualValue = await getSSMParameterValue(parameterName);
      assert.strictEqual(isValidJSON(actualValue),true);
    } catch (error) {
      throw error;
    }
  });
});
function isValidJSON(str:string) {
    try {
        console.log(str);
      JSON.parse(str);
      return true;
    } catch (error) {
      return false;
    }
  }